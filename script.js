import Api from './Api.js';

let listof = document.querySelector("ul");
const button = document.getElementById("buttons");
const Popular = document.getElementById("Popular");
const Bookmarks = document.getElementById("Bookmarks") 

const readLocalStorage = () => {
  let arr = localStorage.getItem("id");
  arr = JSON.parse(arr);
  if (arr === null) {
    arr = [];
  }
  return arr;
}; 

const setLocalStorage = (id) => {
  let arr = readLocalStorage();
  const index = arr.indexOf(id);
  if (index === -1) {
    arr.push(id);
  } else {
    arr.splice(index, 1);
  }
  return localStorage.setItem("id", JSON.stringify(arr));
};

const renderPopularMovies = async (isLiked) => {
  const loading = document.getElementById("load");
  try {
    const fetchedFilms = await Api.fetchPopularMovies();

    const listof = document.querySelector("ul");
    const popularFilm = document.createElement("li");

    fetchedFilms.map((film) => {
      const popularFilm = document.createElement("li");

      const arrid = readLocalStorage();
      const { id, poster_path, original_title } = film;
      popularFilm.dataset.id = id;

      popularFilm.innerHTML = `
        <h3>${original_title}</h3>
        <img src="https://www.themoviedb.org/t/p/w200/${poster_path}" alt="${film.overview}">
        <a href="#" class="like-button ${arrid.includes(""+id) ? "like-button-active" : ""} ">
          <i class="fas fa-heart"></i>
        </a>
        `;

      if (isLiked) {
        if (arrid.includes(""+id)) {
          listof.append(popularFilm);
      }} else listof.append(popularFilm);
    });

  } catch (err) {
    console.log(err);
  } finally {
    //loading.remove();
  }
};

button.addEventListener("click", (evt) => {

  const buttonElement = evt.target.closest("button");

  if (buttonElement === Popular) {

    Popular.classList.add("button-active");
    Bookmarks.classList.remove("button-active");

    listof.innerHTML = ``;
    renderPopularMovies(false);

  } else {

    Popular.classList.remove("button-active");
    Bookmarks.classList.add("button-active");

    listof.innerHTML = ``;
    renderPopularMovies(true);

  }
})

if (Popular.classList.contains("button-active")) {
  renderPopularMovies(false);
}

const list = document.getElementById("list");
list.addEventListener("click", (evt) => {
  const buttonElement = evt.target.closest("a.like-button");
  
  if (buttonElement !== null) {
    evt.preventDefault();
    buttonElement.classList.toggle("like-button-active");
    setLocalStorage(evt.target.closest("li").dataset.id);
    
    if (Bookmarks.classList.contains("button-active")) {
      listof.innerHTML = ``;
      renderPopularMovies(true);
    }
    return;
  }
});