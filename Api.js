class Api {
    constructor() {
        this.url = 'https://api.themoviedb.org/3/movie/popular?api_key=9ab4e0c0c4d3ba62a8ae20bc1aaa38f1';
        this.headers = { "Content-Type": "application/json" };
    }

    async fetchPopularMovies() {
        try {
            const res = await fetch(this.url, {
                headers: this.headers
            });

            const data = await res.json();
            return data.results;
        } catch (err) {
            console.log("Error ", err);
        }
    }
}

export default new Api;